package com.common.steps.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.common.logger.Logger;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;

/**
 * @author ADMIN This class contains all the common steps required to interact
 *         with web browser List of methods is as follows getBaseURL,
 *         closerDriver,quitDriver,clickAnchorByXpath,
 *         clickAnchorByXpathIfPresent,enterDataByXpath,getKeyValue,
 *         verifyTextPresentByXpath,
 *         verifyCheckboxIsCheckedByXpath,verifyCheckboxIsNotCheckedByXpath,
 *         selectVisibleValueByXpath,selectIndexValueByXpath,clickAnchorById
 * 
 */
public class CommonSteps {

	WebDriver driver;
	WebElement element;
	List<WebElement> elementList;

	/**
	 * This method selects the base url and desired browser to be used Setup
	 * currently done only for IE and fire fox. Chrome is giving some issues
	 */

	public void getBaseURL(String browserName, String urlKey) {

		String tempBrowserName = browserName.trim();
		String tempUrl = getKeyValue(urlKey);
		Logger.CONSOLE.info("Setting up browser...");
		if (tempBrowserName.equalsIgnoreCase("chrome")) {
			String tempDriverPath = getKeyValue("CHROME_DRIVER_PATH");
			System.setProperty("webdriver.chrome.driver", tempDriverPath);
			Logger.CONSOLE.info("Chrome Browser set properties successful");
			driver = new ChromeDriver();
			Logger.CONSOLE.info("Initializing Chrome Web Driver");
			Logger.CONSOLE.info("Deleteing Cookies");
			driver.manage().deleteAllCookies();
			Logger.CONSOLE.info("Fetching specified url = " + tempUrl);
			driver.get(tempUrl);
			driver.manage().window().maximize();
			Logger.CONSOLE.info("Successfully opened chrome browser with specified url");
		} else if (tempBrowserName.equalsIgnoreCase("ie")) {
			String tempDriverPath = getKeyValue("IE_DRIVER_PATH");
			Logger.CONSOLE.info("Fetching Driver path specified as - " + tempDriverPath);
			System.setProperty("webdriver.ie.driver", tempDriverPath);
			Logger.CONSOLE.info("IE Browser set properties successful");
			driver = new InternetExplorerDriver();
			Logger.CONSOLE.info("Initializing IE Web Driver");
			Logger.CONSOLE.info("Deleteing Cookies");
			driver.manage().deleteAllCookies();
			Logger.CONSOLE.info("Fetching specified url = " + tempUrl);
			driver.get(tempUrl);
			driver.manage().window().maximize();
			Logger.CONSOLE.info("Successfully opened IE browser with specified url");
		} else if (tempBrowserName.equalsIgnoreCase("firefox")) {
			Logger.CONSOLE.info("Initializing FireFox Web Driver");
			driver = new FirefoxDriver();
			Logger.CONSOLE.info("Deleteing Cookies");
			driver.manage().deleteAllCookies();
			Logger.CONSOLE.info("Fetching specified url = " + tempUrl);
			driver.get(tempUrl);
			driver.manage().window().maximize();
			Logger.CONSOLE.info("Successfully opened Fire fox  browser with specified url");
		} else {
			Logger.CONSOLE
					.error(" Browser name not matched. Please verify browser name again as : firefox, ie or chrome ");
		}

	}
	// ********************************************************************************************************************************************************
	/**
	 * Below methods is generic which is used internally These method are not
	 * useful for user
	 */
	public String getKeyValue(String key) {

		Properties pro = new Properties();
		String value = null;
		InputStream inputStream = null;

		String fileLocation = "src\\main\\java\\locators\\locators.properties";
		try {

			Logger.CONSOLE.info("Loading property file...");
			inputStream = new FileInputStream(fileLocation);
			pro.load(inputStream);
			Logger.CONSOLE.info("File loaded successfully.");
			Logger.CONSOLE.info("Feaching value from property of key - " + key);
			value = pro.getProperty(key);
			Logger.CONSOLE.info("Feaching Successfull");
		} catch (IOException e) {
			Logger.CONSOLE.error(
					"Exception: Unable to load prpoerty file from the location. Please re verify the mentioned location");
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					Logger.CONSOLE.error("Exception: Unable to close File input stream.");
					e.printStackTrace();
				}

			}
		}
		return value;
	}

	/**
	 * This method checks whether driver is null or not if found to be not null
	 * then it will close the current focused driver
	 */
	public void closerDriver() {

		if (driver != null) {
			driver.close();
			Logger.CONSOLE.info("Successfully closed driver");
		} else

			Logger.CONSOLE.info("Driver found to be null");
	}

	/**
	 * This method will check driver not null and if true then will use command
	 * quit and will close all the opened driver instances
	 */
	public void quitDriver() {

		if (driver != null) {
			driver.quit();
			Logger.CONSOLE.info("Successfully closed driver");
		} else

			Logger.CONSOLE.info("Driver found to be null");

	}

	/**
	 * This method will select value from drop down depending upon text
	 * specified in the method
	 */
	public void selectVisibleValueByXpath(String elementDescription, String key, String textToSelect) {
		String tempXpath = getKeyValue(key);
		element = driver.findElement(By.xpath(tempXpath));
		Select dropValue = new Select(element);
		dropValue.selectByVisibleText(textToSelect);
	}

	/**
	 * This method will select value from drop down depending upon index
	 * specefied
	 */
	public void selectIndexValueByXpath(String elementDescription, String key, int indexOfValue) {
		String tempXpath = getKeyValue(key);
		element = driver.findElement(By.xpath(tempXpath));
		Select dropValue = new Select(element);
		dropValue.selectByIndex(indexOfValue);
	}

	/**
	 * This methods performs click operation on the specified Xpath
	 * 
	 * @param elementDescription
	 * @param key
	 */

	public void clickAnchorByXpath(String elementDescription, String key) {

		Logger.CONSOLE.info("Fetching Xpath...");
		String tempXpath = getKeyValue(key);
		Logger.CONSOLE.info("Performing Click operation on Xpath - " + tempXpath);
		element = driver.findElement(By.xpath(tempXpath));
		if (element != null) {
			// element.clear();
			element.click();
		} else
			Logger.CONSOLE.info("Element not found. Verify Xpath - " + tempXpath);

	}

	/**
	 * This method will check whether Xpath is present or not. If true will
	 * click or else will Continue
	 * 
	 * PENDING NOT SURE WITH ELSE PART
	 */

	public void clickAnchorByXpathIfPresent(String elementDescription) {

	}

	/**
	 * This method performs Send Keys operation on the specified Xpath
	 * 
	 * @param elementDescription
	 * @param key
	 * @param data
	 */
	public void enterDataByXpath(String elementDescription, String key, String data) {

		Logger.CONSOLE.info("Fetching Xpath...");
		String tempXpath = getKeyValue(key);
		Logger.CONSOLE.info("Performing Send Keys operation on Xpath - " + tempXpath);
		element = driver.findElement(By.xpath(tempXpath));
		if (element != null) {
			// element.clear();
			element.sendKeys(data);
		} else
			Logger.CONSOLE.info("Element not found. Verify Xpath - " + tempXpath);

	}

	/**
	 * This method performs assert operation between actual and expected text
	 * Tested : Able to handle exception. Working as expected
	 * asertEquals method from TestNG.org
	 * 
	 * @param elementDescription
	 * @param key
	 * @param textDataKey
	 */
	public void verifyTextPresentByXpath(String elementDescription, String key, String textDataKey) {

		Logger.CONSOLE.info("Fetching Xpath...");
		String tempXpath = getKeyValue(key);
		String tempExpectedText = getKeyValue(textDataKey).trim();
		String actualText;
		Logger.CONSOLE.info("Verifying element of Xpath " + tempXpath);
		element = driver.findElement(By.xpath(tempXpath));
		if (element != null) {

			Logger.CONSOLE.info("Element Found...");
			actualText = element.getText();
			Logger.CONSOLE.info("Performing Assert operation between actual and expected text");
			Logger.CONSOLE.info("Actual text from browser is -" + actualText);
			Logger.CONSOLE.info("Expected text is - " + tempExpectedText);
			org.testng.Assert.assertEquals(actualText, tempExpectedText);
			Logger.CONSOLE.info("Text is matching");

		} else
			Logger.CONSOLE.info("Element Found to be Null. Please check locator again.");

	}

	/**
	 * This method will check weather element is checked of the specified Xpath
	 * And if Found true then will run further and if found false then will
	 * throw an exception as "Check box not checked"
	 */
	public void verifyCheckboxIsCheckedByXpath(String elementDescrition, String xpath) {

		String tempXpath = getKeyValue(xpath);
		Logger.CONSOLE.info("Fetching Xpath..." + tempXpath);
		element = driver.findElement(By.xpath(tempXpath));
		Logger.CONSOLE.info("Locating element...");
		boolean result = element.isSelected();
		Logger.CONSOLE.info("Verifying check box is checked or not");
		if (result == true) {
			Assert.assertEquals(result, true);
			Logger.CONSOLE.info("Check box is checked");
		} else {
			try {
				Logger.CONSOLE.error("Check box found Unchecked");
				throw new Exception("Check box not checked");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method will verify check box NOT checked. if true then NOExp and if
	 * returned false then Exception as "Check box is checked"
	 */

	public void verifyCheckboxIsNotCheckedByXpath(String elementDescrition, String xpath) {
		String tempXpath = getKeyValue(xpath);
		Logger.CONSOLE.info("Fetching Xpath..." + tempXpath);
		element = driver.findElement(By.xpath(tempXpath));
		Logger.CONSOLE.info("Locating element...");
		boolean result = element.isSelected();
		Logger.CONSOLE.info("Verifying check box is checked or not");
		if (result == false) {
			Assert.assertEquals(result, false);
			Logger.CONSOLE.info("Check box is Not checked");
		} else {
			try {
				Logger.CONSOLE.error("Check box found checked");
				throw new Exception("Check box is checked");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	/**
	 * This method will perform click option on element specified by id 
	 * @param elementDescription
	 * @param key
	 */
	public void clickAnchorById(String elementDescription, String key){
		String tempId= getKeyValue(key);
		try{
		Logger.CONSOLE.info("Fetching id ..."+tempId);
		Logger.CONSOLE.info("Performing operation on id ");
		driver.findElement(By.id(tempId)).click();
		}catch(ElementNotFoundException e){
			Logger.CONSOLE.error("Error: Unable to Find element by Id" +tempId);
			e.printStackTrace();
		}
	}
}
