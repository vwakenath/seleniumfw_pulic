package com.testsuite;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.common.steps.web.CommonSteps;

public class TestRedifRegistration {
	CommonSteps step = new CommonSteps();
	
	@Test (priority=2)
	public void RedifRegistration() {
		step.getBaseURL("firefox", "REDIF_REGISTRATION_URL");
		step.enterDataByXpath("full name", "REDIF_FULL_NAME_XPATH", "Hardcoded");
		step.enterDataByXpath("operating mail id", "REDIF_CHOOSE_REDID_XPATH", "checknew123");
		step.enterDataByXpath("set passowrd", "REDIF_PASSWORD_XPATH", "pass@word");
		step.enterDataByXpath("confirm password", "REDIF_CONFIRM_PASSWORD_XPATH", "pass@word");
		step.enterDataByXpath("alternate email", "REDIF_ALTER_EMAIL_XPATH", "vn@gmail.com");
		step.enterDataByXpath("mobile number", "REDIF_MOBILE_NO_XPATH", "0123456789");
		step.selectVisibleValueByXpath("day dob", "REDIF_DOB_DAY_XPATH", "10");
		step.selectVisibleValueByXpath("dob month", "REDIF_DOB_MONTH_XPATH", "JAN");
		step.selectVisibleValueByXpath("dob year", "REDIF_DOB_YEAR_XPATH", "1992");
	}
	
	@Test (enabled=false)
	public void creatingNewValidiations(){
		System.out.println("Into test ********************************************* redif");
		step.getBaseURL("firefox", "REDIF_REGISTRATION_URL");
		step.verifyTextPresentByXpath("Page title text", "REDIF_TITLE_LANING_PAGE","REDIF_TITLE_TEXT" );
	}
	
	@AfterMethod
	public void CloseDriver(){
		step.closerDriver();
	}
}
